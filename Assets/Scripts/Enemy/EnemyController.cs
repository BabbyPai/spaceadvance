﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        private GameObject player;
        private void Start()
        {
            player = GameObject.FindWithTag("Player");
            InvokeRepeating("OnFire", 1, 1.5f);
        }
        private void Update()
        {
            MoveToPlayer();
        }
        
        private void OnFire()
        {
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
             var directionToPlayer = player.transform.position - transform.position;
             var velocity = directionToPlayer.normalized * enemySpaceship.Speed;

             transform.Translate(new Vector3(velocity.x,0,0) * Time.deltaTime);
         }
    }    
}

