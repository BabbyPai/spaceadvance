using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        [SerializeField] private Bullet plasmaBullet;
        public event Action OnExploded;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(plasmaBullet != null, "plasmaBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(audioSource != null,"audioSource cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerFire);
        }

        public void PlasmaFire()
        {
            var bullet = Instantiate(plasmaBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerPlasma);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.PlayPlayerDeath();
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}