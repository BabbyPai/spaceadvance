using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.PlayEnemyDeath();
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
        
        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Uturn();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.EnemyFire);
        }
        
    }
}