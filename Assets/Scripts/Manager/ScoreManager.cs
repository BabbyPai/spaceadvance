﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        private GameManager gameManager;
        
        public void Init(GameManager gameManager)
        {
            this.gameManager = gameManager;
            this.gameManager.OnRestarted += OnRestarted;
            UIManager.Instance.HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            UIManager.Instance.SetText($"Score : {score}");
        }
        
        private void OnRestarted()
        {
            gameManager.OnRestarted -= OnRestarted;
            UIManager.Instance.HideScore(true);
            SetScore(0);
        }
        
    }
}


