﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private Button startButton;
    [SerializeField] private Button nextButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private RectTransform dialog;
    [SerializeField] private TextMeshProUGUI scoreText;
    private void Awake()
    {
        Debug.Assert(startButton != null, "startButton cannot be null");
        Debug.Assert(exitButton != null, "exitButton cannot be null");
        Debug.Assert(dialog != null, "dialog cannot be null");
        Debug.Assert(scoreText != null, "scoreText cannot null");
        
        startButton.onClick.AddListener(OnStartButtonClicked);
        exitButton.onClick.AddListener(OnExitButtonClicked);
        nextButton.onClick.AddListener(OnNextButtonClicked);
        
        nextButton.gameObject.SetActive(false);
    }
    
    private void OnStartButtonClicked()
    {
        dialog.gameObject.SetActive(false);
        GameManager.Instance.StartGame();
    }

    public void InGameWin()
    {
        dialog.gameObject.SetActive(true);
        startButton.gameObject.SetActive(false);
        nextButton.gameObject.SetActive(true);
    }

    public void InGameLose()
    {
        dialog.gameObject.SetActive(true);
    }

    private void OnExitButtonClicked()
    {
        Application.Quit();
    }

    private void OnNextButtonClicked()
    {
        SceneManager.LoadScene("Stage2");
    }
    
    public void HideScore(bool hide)
    {
        scoreText.gameObject.SetActive(!hide);
    }

    public void SetText(string text)
    {
        scoreText.text = text;
    }
}

