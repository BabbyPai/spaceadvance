﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        public event Action OnRestarted;
        private PlayerSpaceship spaceship;
        private EnemySpaceship enemyship;
        [SerializeField] private int enemySpacesphipHP;
        private int enemyCount;
        private int score;
        
        private void Awake()
        {
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            
            enemyCount = 1;
            score = 0;
        }

        private void Update()
        {
            if (enemyCount == 0)
            {
                Win();
            }
            
        }

        public void StartGame()
        {
            ScoreManager.Instance.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SoundManager.Instance.PlayBGM();
        }
        
        private void SpawnPlayerSpaceship()
        {
            spaceship = Instantiate(playerSpaceship);
            spaceship.Init(100,10f);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            enemyship = Instantiate(enemySpaceship);
            enemyship.Init(enemySpacesphipHP,20f);
            enemyship.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            score++;
            ScoreManager.Instance.SetScore(score);
            enemyCount--;
        }

        private void Restart()
        {
            OnRestarted?.Invoke();
            Lose();
        }

        public void Win()
        {
            spaceship.Delete();
            UIManager.Instance.InGameWin();
            
        }

        public void Lose()
        {
            enemyship.Delete();
            UIManager.Instance.InGameLose();
        }

    }
}
